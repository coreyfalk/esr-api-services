# ESR Services #

This project contains ESR WSO2 services. 

### Project Structure ###

- **esr-parent-build-all** Project that builds all the services in the correct order.
- **esr-parent** Parent project that contains common properties like ESB host/port, maven distribution management etc
- **RhapsodyAPI** Contains all static artifacts
- **RhapsodyAPIRegistry** Contains dynamic artifacts that change per environment
- **RhapsodyAPICompositeApplication** Project that creates CAR file
- **Test** This folder contains Starlims mock services and SoapUI unit tests for testing Forensic APIs developed in ESB

### Clone and Create Workspace ###

Clone the existing repository and import projects into eclipse as maven projects. Provide `esr-services` as the base folder to import the projects. If necessary, right click a project and select `Configure->Convert to Maven Project`.

### Run and Deploy ###

####Maven install####

```
mvn clean install
```

####Maven deploy####

In order to deploy CAR file directly to ESB using maven, we need to copy ESB's keystore to esr-parent-build-all project. The repository currently comes with default ESB 5.0.0 keystore.

````
cp <ESB_HOME>/repository/resources/security/wso2carbon.jks <ESR_Workspace>/esr-parent-build-all/src/main/resources
````

Then give the following command from esr-parent-build-all project

```
mvn clean deploy -Dmaven.deploy.skip=true -Dmaven.car.deploy.skip=false
```

ESB logs could be checked for deployment status

```
Successfully Deployed Carbon Application : RhapsodyAPICompositeApplication_1.0.0
```

### Error Handling ###
`GenericFaultHandler_SQ` sequence is added to RhapsodyAPI project that should be invoked with appropriate ERROR_MESSAGE and ERROR_DETAIL properties, whenever there is a business/input error. This sequence will automatically get invoked on mediation errors with correct properties. All sequence's onError field should point to `GenericFaultHandler_SQ`.

### Updating Tests ###
Follow the steps given below to update/ add test cases.

    1.  Import `esr-services/Test/SoapUIUnitTests/src/test/resources/ESR-RhapsodyAPI-soapui-project.xml` to SoapUI
    2.  Check the custom properties to make sure it points to the correct ESB against which you want to run the tests
    3.  Add test cases for the required APIs
    4.  Add any new properties to `esr-services/Test/SoapUIUnitTests/pom.xml`
    5.  Run tests locally and push the changes to Bitbucket. Make sure that the latest soapui project is pushed to the repository. Jenkins job should pick it up and run them on dev  